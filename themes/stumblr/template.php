<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * stumblr theme.
 */

/**
 * {@inheritdoc}
 */
function stumblr_preprocess_html(&$variables) {
  drupal_add_css('http://fonts.googleapis.com/css?family=Lato', array('type' => 'external'));
}

/**
 * Override to remove the grippie from the textarea.
 */
function stumblr_textarea($variables) {
  $element = $variables['element'];
  $element['#attributes']['name'] = $element['#name'];
  $element['#attributes']['id'] = $element['#id'];
  $element['#attributes']['cols'] = $element['#cols'];
  $element['#attributes']['rows'] = $element['#rows'];
  _form_set_class($element, array('form-textarea'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    $wrapper_attributes['class'][] = 'resizable';
  }

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
  $output .= '</div>';
  return $output;
}

/**
 * hook_preprocess_node()
 *
 * Sanitize user input to prevent XSS exploits.
 * see https://www.drupal.org/node/2203947#comment-9303275
 */
function stumblr_preprocess_node(&$variables) {
  if($variables['type'] == 'stumblr_post'){
    $variables['title_preprocess'] = l($variables['title'],
      ltrim($variables['node_url'], '/'),
      array('attributes' => array('rel' => 'bookmark')));

    if (!empty($variables['field_tags'])){
      $tags_html = array();
      foreach($variables['field_tags'] as $tag){
        $tags_html[] = l($tag['taxonomy_term']->name, "taxonomy/term/" . $tag['tid']);
      }
      $variables['tags_preprocess'] = implode(', ', $tags_html);
    }else {
      $variables['tags_preprocess'] = '';
    }
  }
}
