CONTENTS OF THIS FILE
---------------------
 * Introduction.
 * Maintainers.

Stumblr is a slick, clean Tumblr style Omega 4 sub-theme which is ideal for
Microblogging photos and videos. It is distributed as an install profile with
all non-visual functionality provided as features.

MAINTAINERS
-----------
Current maintainer:
 * Riadh Habbachi (https://drupal.org/user/2785217) - Original author.
 
This project has been sponsored by:
* Angry Cactus Technoloy (https://angrycactus.biz).
