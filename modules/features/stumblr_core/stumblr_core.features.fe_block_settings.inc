<?php
/**
 * @file
 * stumblr_core.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function stumblr_core_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-about'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'about',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => -11,
      ),
    ),
    'title' => 'About',
    'visibility' => 0,
  );

  $export['block-copyrights'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'copyrights',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'copyright',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => -10,
      ),
    ),
    'title' => 'Copyrights',
    'visibility' => 0,
  );

  $export['block-footer_links'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_links',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => 1,
      ),
    ),
    'title' => 'Links',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => 0,
      ),
    ),
    'title' => 'Search',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => -9,
      ),
    ),
    'title' => 'Custom Menu',
    'visibility' => 0,
  );

  $export['views-categories-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'categories-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'stumblr' => array(
        'region' => 'sidebar',
        'status' => 1,
        'theme' => 'stumblr',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
