<?php
/**
 * @file
 * stumblr_core.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function stumblr_core_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'About summary';
  $fe_block_boxes->format = 'stumblr_html_content';
  $fe_block_boxes->machine_name = 'about';
  $fe_block_boxes->body = '<p>This is a demo of the <a href="http://www.angrycactus.biz/stumblr-theme/">Stumblr Drupal theme</a> from Angry Cactus.</p>';

  $export['about'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Show copyrights for the theme, site...';
  $fe_block_boxes->format = 'stumblr_html_content';
  $fe_block_boxes->machine_name = 'copyrights';
  $fe_block_boxes->body = '<p>&copy; 2014 Stumblr Theme Demo | Theme by Angry Cactus.</p>';

  $export['copyrights'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'List of links related to the theme.';
  $fe_block_boxes->format = 'stumblr_html_content';
  $fe_block_boxes->machine_name = 'footer_links';
  $fe_block_boxes->body = '<div class="textwidget"><ul><li><a href="http://www.eleventhemes.com/stumblr-theme/" target="_blank">Download Stumblr (Free)</a></li><li><a href="https://twitter.com/#!/eleventhemes" target="_blank">Follow Angry Cactus on Twitter</a></li><li><a href="http://www.facebook.com/pages/Eleven-Themes/350230314995149" target="_blank">Like Angry Cactus on Facebook </a></li></ul></div>';

  $export['footer_links'] = $fe_block_boxes;

  return $export;
}
